/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolution.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmeyer <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 13:54:22 by tmeyer            #+#    #+#             */
/*   Updated: 2019/01/07 12:14:49 by tmeyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static char		**uncover_case(char **print, char **map, int i, int j)
{

	if (i < 0 || j < 0 || map[i] == NULL || map[i][j] == '\0')
	   return (print);
	if (map[i][j] != '0' && map[i][j] != '*' && map[i][j] != 32)
		print[i][j] = map[i][j];
	if (map[i][j] == '0')
	{
		map[i][j] = 32;
		print[i][j] = 32;
		print = uncover_case(print, map, i - 1, j - 1);
		print = uncover_case(print, map, i - 1, j);
		print = uncover_case(print, map, i - 1, j + 1);
		print = uncover_case(print, map, i, j - 1);
		print = uncover_case(print, map, i, j + 1);
		print = uncover_case(print, map, i + 1, j - 1);
		print = uncover_case(print, map, i + 1, j);
		print = uncover_case(print, map, i + 1, j + 1);
	}
	return (print);
}

static int		count_uncovered(char **print)
{
	int i;
	int j;
	int k;

	i = 0;
	k = 0;
	while (print[i] != NULL)
	{
		j = 0;
		while (print[i][j] != '\0')
		{
			if (print[i][j] == '!')
				k++;
			j++;
		}
		i++;
	}
	return (k);
}

static char		**make_print(char **print, int x, int y)
{
	int i;
	int j;

	if (!(print = (char**)malloc(sizeof(char*) * (y + 1))))
		return (NULL);
	i = 0;
	while (i < y)
	{
		if (!(print[i] = (char*)malloc(sizeof(char) * (x + 1))))
			return (NULL);
		j = 0;
		while (j < x)
			print[i][j++] = 'X';
		i++;
	}
	return (print);	
}

int				resolution(char **map, int x, int y, int limit)
{
	char	**print;
	char		t[2];
	int			s[2];
	int		k;
	char	a;

	k = 0;
	print = NULL;
	if (!(print = make_print(print, x, y)))
	{
		free_map(print);
		return (0);
	}
	printf("Coordinates plus action\nu : uncover\nm : mark a mine\nq : set a"
			"question mark\nExemple : acu will uncover the (A,C) box\n");
	print_map(print);
	while (k != limit)
	{
		scanf(" %c %c %c", &t[1], &t[0], &a);
		s[0] = (int)(t[0]) - 'a';
		s[1] = (int)(t[1]) - 'a';
		if (s[0] >= y || s[1] >= x)
			a = 'w';
		if (map[s[0]][s[1]] == '*' && a == 'u')
		{
			free_map(print);
			return (0);
		}
		switch (a)
		{
			case 'm' : print[s[0]][s[1]] = (print[s[0]][s[1]] == '!' ? 'X' : '!');
					   break ;			
			case 'q' : print[s[0]][s[1]] = (print[s[0]][s[1]] == '?' ? 'X' : '?');
					   break ;
			case 'u' : print = uncover_case(print, map, s[0], s[1]);
					   break ;
			default : printf("Coordinates plus action\nu : uncover\nm :"
							  "mark a mine\nq : set a question mark\n"
							  "Exemple : acu will uncover the (A,C) box\n");
					  break ;
		}
		k = count_uncovered(print);
		if (a == 'm' || a == 'q' || a == 'u')
			print_map(print);
	}
	free_map(print);
	printf("\n\n GREAT JOB \n\n");
	return (1);
}
