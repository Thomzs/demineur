/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmeyer <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 11:02:58 by tmeyer            #+#    #+#             */
/*   Updated: 2018/12/14 16:00:10 by tmeyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "libft/libft.h"

# define COLOR_B_MAGENTA	"\033[1;35m"
# define COLOR_RED			"\033[31m"
# define COLOR_GREEN		"\033[32m"
# define COLOR_YELLOW		"\033[33m"
# define COLOR_BLUE			"\033[34m"
# define COLOR_MAGENTA		"\033[35m"
# define COLOR_B_YELLOW		"\033[01;33m"
# define COLOR_B_RED		"\033[1;31m" 
# define COLOR_CYAN			"\033[36m"
# define COLOR_RESET		"\033[0m"

# define debug1 write(1, "ICI\n", 4);

char		**bomb_placement(int limit, int x, int y, char **map);
void		free_map(char **map);
char		**count_mines(char **map, int x, int y);
char		**increment(char **map, int i, int j);
char		*color(char c);
char		**reset_zero(char **map, int x, int y);
int			resolution(char **map, int x, int y, int limit);
void		print_map(char **map);

#endif
