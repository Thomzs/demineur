/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmeyer <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 11:02:14 by tmeyer            #+#    #+#             */
/*   Updated: 2019/01/07 11:58:56 by tmeyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static char		**do_map(char **map, int x, int y, int limit)
{
	int i;
	int j;

	if (!(map = (char**)malloc(sizeof(char*) * (y + 1))))
		return (NULL);
	i = 0;
	while (i < y)
	{
		if (!(map[i] = (char*)ft_memalloc(x + 1)))
			return (NULL);
		j = 0;
		while (j < x)
			map[i][j++] = '0';
		i++;
	}
	map[i] = NULL;
	map = bomb_placement(limit, x, y, map);
	map = count_mines(map, x, y);
	return (map);
}

void			print_map(char **map)
{
	size_t i;
	size_t j;
	char c;

	c = 'A';
	i = 0;
	j = 0;
	printf("\n\n\t   |");
	while (++j < ft_strlen(map[0]) + 1)
		printf(" %c |", c++);
	printf("\n\t");
	c = 'A';
	while (map[i] != NULL)
	{	
		j = 0;
		while (j++ < ft_strlen(map[0]) + 1)
			printf(" -  ");
		printf("\n\t %c |", c++);
		j = 0;
		while (map[i][j] != '\0')
		{
			printf("%s %c %s|", color(map[i][j]), map[i][j], COLOR_RESET);
			j++;
		}
		printf("\n\t"); 
		i++;
	}
	j = 0;
	while (j++ < ft_strlen(map[0]) + 1)
		printf(" -  ");
	printf("\n\n\n");
}

int			main(int ac, char **av)
{
	char **map;
	int x;
	int y;
	int limit;
	
	if (ac != 4)
		printf("usage : ./demineur taille taille densite\n note : taille should be < 10\n");
	x = ft_atoi(av[1]);
	y = ft_atoi(av[2]);
	limit = ft_atoi(av[3]);
	map = NULL;
	srand(time(NULL));
	if (!(map = do_map(map, x, y, limit)))
	{
		free_map(map);
		exit(0);
	}
	if (!(resolution(map, x, y, limit)))
	{		
		printf("\n\n Too bad \n\n");
		map = reset_zero(map, x, y);
		print_map(map);
	}
	free_map(map);
	exit(1);
	return (0);
}
