/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmeyer <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 13:10:17 by tmeyer            #+#    #+#             */
/*   Updated: 2019/01/07 12:05:25 by tmeyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

char 		*color(char c)	
{
	switch (c)
	{
		case '1' : return (COLOR_BLUE);
		case '2' : return (COLOR_CYAN);
		case '3' : return (COLOR_GREEN);
		case '4' : return (COLOR_MAGENTA);
		case '5' : return (COLOR_B_MAGENTA);
		case '6' : return (COLOR_B_RED);
		case '7' : return (COLOR_RED);
		case '8' : return (COLOR_YELLOW);
	}
	return ("");
}

char		**bomb_placement(int limit, int x, int y, char **map)
{
	int	i;
	int	j;
	int	k;

	k = 0;
	while (k < limit)
	{
		i = rand() % x;
		j = rand() % y;
		if (map[i][j] != '*')
		{
			map[i][j] = '*';
			k++;
		}
	}
	return (map);
}

char		**increment(char **map, int i, int j)
{
	int k;
	int l;

	k = (i - 1 < 0 ? 0 : i - 1);
	while (k < (i - 1 < 0 ? 2 : i + 2) && map[k] != NULL)
	{
		l = (j - 1 < 0 ? 0 : j - 1);
		while (l < (j - 1 < 0 ? 2 : j + 2) && map[k][l] != '\0')
		{
			if (map[k][l] != '*')
				map[k][l]++;
			l++;
		}
		k++;
	}
	return (map);
}

char		**count_mines(char **map, int x, int y)
{
	int i;
	int j;

	i = 0;
	while (i < y)
	{
		j = 0;
		while (j < x)
		{
			if (map[i][j] == '*')
				map = increment(map, i, j);	
			j++;
		}
		i++;
	}
	return (map);
}

void		free_map(char **map)
{
	int i;

	//	if (map)
	//	{	
	i = 0;
	while (map[i])
		free(map[i++]);
	free(map);
	//	}
}
